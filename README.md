# Make Matplotlib plots in Jupyter notebooks using a context manager API

See [`example.ipynb`](https://gitlab.com/joshburkart/contextplot/-/blob/main/example.ipynb).
